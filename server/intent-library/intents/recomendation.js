/**
 * Copyright 2020 Quantiphi, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

"use strict";

/**
 * Default Welcome Intent controller
 * @param {object} df webhook fulfillment object
 */

const isTelephony = require('../../helper/telephony')

const recommendation = async (df) =>{

  if(isTelephony(df)){

    df.setSynthesizeSpeech('<speak>'+
    'What genre of content do you like to watch the most? You can say Drama, Comedy, Thriller, Romance, or Horror'+
    '</speak>')
  }else{
    df.setResponseText("What genre of content do you like to watch the most?");
  
    df.setPayload({
        richContent: [
          [
            {
              type: "chips",
              options: [
                {
                  text: "Drama",
                },
                {
                  text: "Thriller",
                },
                {
                    text: "Romance",
                },
                {
                    text: "Horror",
                 },
                 {
                    text: "Comedy",
                 }
              ],
            },
          ],
        ],
      });
      
};
 }


module.exports = recommendation;
