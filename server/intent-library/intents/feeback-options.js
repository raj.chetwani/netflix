/**
 * Copyright 2020 Quantiphi, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

"use strict";

/**
 * Default Welcome Intent controller
 * @param {object} df webhook fulfillment object
 */
const isTelephony = require('../../helper/telephony')
const feedbackOptions = async (df) =>{
    const rating = df._request.queryResult.parameters.rating

    if(isTelephony(df)){
        if( rating >= 1 && rating <=3 ){
            df.setSynthesizeSpeech('<speak>'+
            "We're sorry to hear that! Would you like to tell us how we can improve?" +
            '</speak>');
        }else{
            df.setSynthesizeSpeech('<speak>'+
            "Thank you for your valuable feedback! Would you like to take a moment to tell us what went well for you?"+
            '</speak>');
        }
    }else{

        if( rating >= 1 && rating <=3 ){
            df.setResponseText("We're sorry to hear that! Would you like to tell us how we can improve?");
        }else{
            df.setResponseText("Thank you for your valuable feedback! Would you like to take a moment to tell us what went well for you?");
        }

    }
   
};

module.exports = feedbackOptions;
