/**
 * Copyright 2020 Quantiphi, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

"use strict";

/**
 * Default Welcome Intent controller
 * @param {object} df webhook fulfillment object
 */
const db = require('../../helper/db')
const isTelephony = require('../../helper/telephony')
const giveRecommendations = async (df) =>{
    const genre = df._request.queryResult.outputContexts[0].parameters.genre
    let recommend = [],movies=[],i=1
    const snapshot = await db.collection('movies').get()

    snapshot.forEach((doc) => {
        if(doc.data().genre === genre){
            const movie ={
                "type": "info",
                "title": doc.data().name,
                "subtitle": `IMDB : ${doc.data().imdb}`,
                "image": {
                  "src": {
                    "rawUrl": doc.data().image
                  }
                }
              }
            
            movies.push(doc.data())
            recommend.push(movie)
        }
    })

    const payload = {
        richContent: [
          recommend
        ]
      }   
     

  if(isTelephony(df)){
    let output = '<speak>'+
    'That was a good one! Based on your choices, here is a list of recommended titles for you to watch.Enjoy!'+ 
    '<break time="1"/>'

      movies.map((movie) => {
          output = output + "Number" + i +'<emphasis level="moderate">'+ movie.name + '</emphasis>' +
             "with a IMDB rating of " + '<emphasis level="moderate">' + movie.imdb + '</emphasis>' +
          '<break time="1"/>'+' . '
          i+=1
  })
    output+='</speak>'
    console.log(output);
    df.setSynthesizeSpeech(output)
  }else{    
    df.setResponseText("That was a good one! Based on your choices, here is a list of recommended titles for you to watch. Enjoy!");
    df.setPayload(payload)
  }

};

module.exports = giveRecommendations;
