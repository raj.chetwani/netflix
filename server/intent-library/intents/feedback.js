/**
 * Copyright 2020 Quantiphi, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

"use strict";

/**
 * Default Welcome Intent controller
 * @param {object} df webhook fulfillment object
 */
const isTelephony = require('../../helper/telephony')
const feedbackIntent = async (df) =>{
    if(isTelephony(df)){
      df.setSynthesizeSpeech('<speak>'+'<break time="0.5"/>'+
      "We're thrilled that you would like to provide feedback! Based on your experience,rate Netflix on the scale of 1 to 5, where 1 being awful and 5 being excellent"+
      '</speak>')
    }else{
      df.setResponseText("We're thrilled that you would like to provide feedback! Based on your experience, how would you rate Netflix?");
      /*  df.setPayload({
            richContent: [
              [
                {
                  type: "chips",
                  options: [
                    {
                      text: "Awful",
                    },
                    {
                      text: "Poor",
                    },
                    {
                        text: "Okay",
                    },
                    {
                        text: "Good",
                     },
                     {
                        text: "Excellent",
                     }
                  ],
                },
              ],
            ],
          });*/
    
          df.setPayload({
            "richContent": [
              [
                {
                  "type": "list",
                  "title": "1 - Awful",
                  "event": {
                    "name": "rating",
                    "languageCode": "",
                    "parameters": {
                      rating:1
                    }
                  }
                },
                {
                  "type": "divider"
                },
                {
                  "type": "list",
                  "title": "2 - Poor",
                  "event": {
                    "name": "rating",
                    "languageCode": "",
                    "parameters": {
                      rating:2
                    }
                  }
                }
                ,
                {
                  "type": "divider"
                },
                {
                  "type": "list",
                  "title": "3 - Okay",
                  "event": {
                    "name": "rating",
                    "languageCode": "",
                    "parameters": {
                      rating:3
                    }
                  }
                }
                ,
                {
                  "type": "divider"
                },
                {
                  "type": "list",
                  "title": "4 - Good",
                  "event": {
                    "name": "rating",
                    "languageCode": "",
                    "parameters": {
                      rating:4
                    }
                  }
                }
                ,
                {
                  "type": "divider"
                },
                {
                  "type": "list",
                  "title": "5 - Excellent",
                  "event": {
                    "name": "good",
                    "languageCode": "",
                    "parameters": {
                      rating:5
                    }
                  }
                }
              ]
            ]
          })
    }
    
};

module.exports = feedbackIntent;
