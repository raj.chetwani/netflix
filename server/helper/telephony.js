const isTelephony = (df) => {
    const detect = df._request.originalDetectIntentRequest
    return (detect && detect.source === "GOOGLE_TELEPHONY" || detect.source==="TELEPHONY")
}

module.exports = isTelephony
