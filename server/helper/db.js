var admin = require("firebase-admin");

var serviceAccount = require("../config/key.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://netflix-1b6f0.firebaseio.com"
});

const db = admin.firestore()

module.exports = db