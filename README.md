# Netflix Bot

## Project Overview
The client wants to develop a virtual agent for answering queries across the chat and telephony platforms.

The scope of the project includes developing a Virtual Agent on DialogFlow ES and writing Webhook Logic for the below mentioned points:

* `Recommendations` based on previous viewings
* `Feedback` for Netflix(1 to 5 options, follow up based on rating)
*  How can I `download a TV show or movie on Netflix`
* How can `I watch Netflix on my TV?`

## Development server

### Frontend 
Execute following commands to run `Angular App` locally
```
cd client
npm install
ng serve -o
```

### Webhook
Execute following commands to run `Node.js (Webhook)` locally
```
cd server
npm install
npm start
```

## Hosted Website

https://netflix-1b6f0.web.app/

## Telephony Number

+1 617-420-1049

## References
* [webhook-boilerplate](https://github.com/Quantiphi/webhook-boilerplate)
* [DF Messenger RichUI and styling](https://cloud.google.com/dialogflow/es/docs/integrations/dialogflow-messenger)
* [Phone Integration and SSML Tags](https://developers.google.com/assistant/conversational/df-asdk/ssml)
