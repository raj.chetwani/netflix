import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import {Movie} from './movie.model'
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  movieList:AngularFirestoreCollection<Movie>
  movies:Observable<Movie[]>


  constructor(private firestore: AngularFirestore) { 
    this.movieList = this.firestore.collection('movies')

    this.movies = this.movieList.valueChanges()
  }

getMovies(){
  return this.movies
}

}
