export interface Movie {
    name?:string;
    imdb?:string;
    image?:string;
    genre?:string
}