import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/movies.service';
@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  constructor(private movieService: MoviesService) { }

  ngOnInit(): void {
    this.movieService.getMovies()
  }

}
